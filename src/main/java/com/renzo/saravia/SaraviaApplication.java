package com.renzo.saravia;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing

public class SaraviaApplication {


    public static void main(String[] args) {
        SpringApplication.run(SaraviaApplication.class, args);
    }

}
