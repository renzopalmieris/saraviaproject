package com.renzo.saravia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/saravia")
public class SaraviaController {

    @Autowired
    private SaraviaService service;





    @RequestMapping(method = RequestMethod.GET,value = "/{SKU}", produces = "application/json")
    public Product findONE(@PathVariable("SKU") String SKU ) {
        return service.findONE(SKU);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/productos", produces = "application/json")
    public List<Product> getAllProductos( ) {
        return service.getAllProductos();
    }


    @RequestMapping(method = RequestMethod.GET,value = "/delete/{SKU}", produces = "application/json")
    public  String Delete(@PathVariable("SKU") String SKU ) {
        return service.Delete(SKU);
    }


/*

    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public Product create(@RequestBody Product geolocation) {
        return service.create(geolocation);
    }


    @RequestMapping(method = RequestMethod.DELETE,value = "/producto/{SKU}", produces = "application/json")
    public ResponseEntity<?> DeleteProduct(@PathVariable("SKU") String SKU ) {
        return service.DeleteProduct(SKU);
    }

*/
}
