package com.renzo.saravia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Product")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)


public class Product implements Serializable{

    @Id
    private  String sku;

    @Column(nullable = false, updatable = false)
    private String name;

    @Column(nullable = false, updatable = false)
    private String brand;

    @Column(nullable = false, updatable = false)
    private int price;


    public Product(){}

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
/*
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
*/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }




    @Override
    public String toString() {
        return "Product{" +
                "sku=" + this.sku +
                ", name='" + this.name + '\''
                //", value='" + this.price + '\''
                ;
    }

}





